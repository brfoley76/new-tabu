Note: the .avi video files were too big for the Bitbucket repository, so I deleted them from these folders. I can send them separately.

In ./TABU/tracking_code the only file you need to worry about is ./TABU/tracking_code/RunScripts.R. See ./tracking_code/readmetxt for details.

All the sample video hour files are named with the following info, separated by underscores:
	year_month_day_view_station_female,genotype,colour,nFemales_male,genotype,color,nMales_period

Within each one there is: 
./<expt>/*.bmp background image
	the patch centroid and diameter have been extracted from each *.bmp, and are all found in ./tracking_code/RunScripts.r
./<expt>/*.avi relevant video
./<expt>/AviFileChunk\d_View\d_\d_-1_out.txt raw blob output, each line corresponds with a separate frame
./<expt>/<expt>_out.csv Ignore this, this is the output from the first round of TABU
./<expt>/testIt_.out.csv the current TABU output. Each line correpsonds with an inferred fly

	frame		frame number (1:108000)
	blobIndex	unique index (within frame) for each blob
	nFlies		number of inferred flies in that blob
	flyFrame	the nth frame for a given trajectory
	blobColour	inferred colour of the the paint
	blobX		blob centroid x
	blobY		blob centroid y
	blobArea	blob area (pixels against background)
	blobAngle	angle of blob ellipse long axis
	blobA		length of blob ellipse long axis
	blobB		length of blob ellipse short axis
	flyID		unique identifier for each fly/trajectory
	deltaX		fly translation in x between frames 
	deltaY		fly translation in y between frames 
	flySpeed	fly dist travelled between frames (smoothed average)
	flyArea		fly inferred area (smoothed average) 
	flyAspect	ratio between ellipse A and B (smoothed average)
	areaDeviance	measure scary shit happening (too much discrepancy between inferred fly area, and blob area)
	framesDeviance	accumulated scary shit tells me something is wrong (smoothed)

./<expt>/join_leave_<expt>.csv the student curated joining and leaving counts 
	minutes 	the minute of the video where something happens
	seconds		the second of the video where something happens
	join_leave	-1 == leave, 1 == join, 0 == nothing (should only be 0 for the first event)
	colour		colour of the fly doing the joining or leaving
	nmales		number of males on the patch after the event
	nfemales	number of females on the patch after the event

./<expt>/filledCurated.csv this is effectively the same as the student curated data, but processed to a per-frame format

./<expt>/comparisonFile.csv a side-by-side comparison of the student-curated and TABU data
	frame		
	minutes		
	seconds		
	nmales		
	nfemales	
	joinLeave	
	curateFlies	the number of flies from the student curated data
	trackedFlies	the number of flies from the TABU data

./<expt>/blue_is_true_threshold.pdf is output showing the number of predicted flies on patch from curateFlies (blue) and trackedFlies (red) by frame

