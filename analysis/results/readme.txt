In the following results, the match between video number and name is as follows:
	video1:	13_02_08_02_2_fby15_mfb5_am2
	video2:	3_03_13_02_2_fcb15_mgy05_am1
	video3:	13_03_15_02_3_fbb5_mfy15_am1
	video4:	13_06_17_1_fbb10_mgy10_am1
	video5:	13_06_18_0_2_fbb10_mey10_am1

NOTE: I changed the order in the paper from the above to go in descending order of sex ratio: 1, 2, 4, 5, 3 (brad). This made table 8 easier to interpret
