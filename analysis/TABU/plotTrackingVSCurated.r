

setwd("/media/Data/Documents/VideoAnalysis/check_assigment_algorithm/13_03_13_02_2_fcb15_mgy05_am1")
curatedFile="join_leave_13_03_13_02_2_fcb15_mgy05_am1.csv"
trackingFile="try_it_with_deviance.out.csv"

curated=read.table(curatedFile, sep=",", as.is=T, header=T)
fliesTracking=read.table(trackingFile, sep=",", as.is=T, header=T)

frame=curated$minutes*1800+curated$seconds*30
curated=cbind(curated, frame)
for(i in 2:nrow(curated)){
  if((curated[i,"minutes"]==curated[i-1,"minutes"]) & (curated[i, "seconds"]==curated[i-1, "seconds"])){
    curated[i,"frame"]=curated[i-1,"frame"]+5
  }
}
curatedLast=curated[nrow(curated),]
curatedLast$frame=108000
curated=rbind(curated, curatedLast)
curated1=curated[1:(nrow(curated)-1),"frame"]
curated2=curated[2:nrow(curated),"frame"]
curated2>curated1

addIntervening=function(nextLeave, lastLeave){
    nFrames=nextLeave$frame-lastLeave$frame
    nextBlock=lastLeave[rep(1, nFrames),]
    nextBlock$frame=c(lastLeave$frame:(nextLeave$frame-1))
    return(nextBlock)   
}

curatedByFrame=do.call(rbind, lapply(1:(nrow(curated)-1), function(k) {addIntervening(curated[k+1,], curated[k,])}))
curatedByFrame=rbind(curatedByFrame, curated[nrow(curated),])
totFlies=curatedByFrame$nmales+curatedByFrame$nfemales
time=curatedByFrame$frame/1800
curatedByFrame=cbind(curatedByFrame, totFlies, time)

count=1
fliesTracking=cbind(fliesTracking, count)
totFliesTracking=aggregate(fliesTracking$count, by=list(fliesTracking[,"frame"]), FUN="sum")
nMeasures=nrow(totFliesTracking)
time=totFliesTracking[,1]/1800
totFliesTracking=cbind(totFliesTracking, time)

plot(totFliesTracking[,2]~totFliesTracking$time, col="red", pch=".", cex=3)
points(curatedByFrame$totFlies[31:nMeasures]~curatedByFrame$time[31:nMeasures], col="blue", pch=".", cex=3)

dev.copy(pdf, "blue_is_true.pdf", width=15, height=5)
dev.off()


plot(totFliesTracking[,2][45000:nMeasures]~totFliesTracking$time[45000:nMeasures], col="red", pch=".", cex=3)
points(curatedByFrame$totFlies[45000:nMeasures]~curatedByFrame$time[45000:nMeasures], col="blue", pch=".", cex=3)

dev.copy(pdf, "blue_is_true_threshold.pdf", width=15, height=5)
dev.off()

plot(totFliesTracking[,2]~curatedByFrame$totFlies[31:107999])
aaa=lm(totFliesTracking[,2][45000:nMeasures]~curatedByFrame$totFlies[45000:nMeasures])
abline(aaa)
summary(aaa)


nextLeave=curated[k+1,]
lastLeave=curated[k,]

