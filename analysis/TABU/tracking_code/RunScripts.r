
######Initialize
codePath="/media/brad/Data/Documents/VideoAnalysis/check_assigment_algorithm/elsevierRewrite/analysis/tracking_code/TABU/"
source("/media/brad/Data/Documents/VideoAnalysis/check_assigment_algorithm/elsevierRewrite/analysis/tracking_code/preprocess.r")
library(lpSolve) # for lp.assign(), hungarian algorithm
library(fields) # for function rdist

######
path="/media/brad/Data/Documents/VideoAnalysis/check_assigment_algorithm/elsevierRewrite/analysis/TABU/13_02_08_02_2_fby15_mfb5_am2"
infile="AviFileChunk1_View3_0_-1_out.txt"
outfile="testIt_out_"
setX=380
setY=301
setDiameter=567
preprocessVideoData(path, infile, outfile, setX, setY, setDiameter)

######
path="/media/brad/Data/Documents/VideoAnalysis/check_assigment_algorithm/elsevierRewrite/analysis/TABU/13_03_13_02_2_fcb15_mgy05_am1"
infile="AviFileChunk0_View2_0_-1_out.txt"
outfile="testIt_out_"
setX=419
setY=295
setDiameter=551
preprocessVideoData(path, infile, outfile, setX, setY, setDiameter)

######
path="/media/brad/Data/Documents/VideoAnalysis/check_assigment_algorithm/elsevierRewrite/analysis/TABU/13_03_15_02_3_fbb5_mfy15_am1/"
infile="AviFileChunk0_View3_0_-1_out.txt"
outfile="testIt_out_"
setX=302.5
setY=287.5
setDiameter=552
preprocessVideoData(path, infile, outfile, setX, setY, setDiameter)
  
######
path="/media/brad/Data/Documents/VideoAnalysis/check_assigment_algorithm/elsevierRewrite/analysis/TABU/13_06_17_1_fbb10_mgy10_am1/"
infile="AviFileChunk0_View1_0_-1_out.txt"
outfile="testIt_out_"
setX=396
setY=298.5
setDiameter=561
preprocessVideoData(path, infile, outfile, setX, setY, setDiameter)
  
######
path="/media/brad/Data/Documents/VideoAnalysis/check_assigment_algorithm/elsevierRewrite/analysis/TABU/13_06_18_0_2_fbb10_mey10_am1/"
infile="AviFileChunk0_View0_0_-1_out.txt"
outfile="testIt_out_"
setX=474
setY=274.5
setDiameter=550
preprocessVideoData(path, infile, outfile, setX, setY, setDiameter)
