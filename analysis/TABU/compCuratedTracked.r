
baseDir="/media/brad/Data/Documents/VideoAnalysis/check_assigment_algorithm/elsevierRewrite/analysis/TABU/videos"

wdList=c("13_02_08_02_2_fby15_mfb5_am2", 
         "13_03_13_02_2_fcb15_mgy05_am1", 
         "13_03_15_02_3_fbb5_mfy15_am1", 
         "13_06_17_1_fbb10_mgy10_am1", 
         "13_06_18_0_2_fbb10_mey10_am1"
         )

curatedFileList=c("join_leave_13_02_08_02_2_fby15_mfb5_am2.csv",
                  "join_leave_13_03_13_02_2_fcb15_mgy05_am1.csv",
                  "join_leave_13_03_15_02_3_fbb5_mfy15_am1.csv",
                  "join_leave_13_06_17_1_fbb10_mgy10_am1.csv",
                  "join_leave_13_06_18_0_fbb10_mey10_am1.csv"
  )

outList=c("13_02_08_02_2_fby15_mfb5_am2.out.csv", 
            "13_03_13_02_2_fcb15_mgy05_am1.out.csv", 
            "13_03_15_02_3_fbb5_mfy15_am1.out.csv", 
            "13_06_17_1_fbb10_mgy10_am1.out.csv", 
            "13_06_18_0_2_fbb10_mey10_am1.out.csv"
)

blobList=c("AviFileChunk1_View3_0_-1_out.txt", 
  "AviFileChunk0_View2_0_-1_out.txt", 
  "AviFileChunk0_View3_0_-1_out.txt", 
  "AviFileChunk0_View1_0_-1_out.txt", 
  "AviFileChunk0_View0_0_-1_out.txt"
  )

addIntervening=function(nextLeave, lastLeave){
  nFrames=nextLeave$frame-lastLeave$frame
  nextBlock=lastLeave[rep(1, nFrames),]
  nextBlock$frame=c(lastLeave$frame:(nextLeave$frame-1))
  if(nFrames>1){
    nextBlock[2:nFrames,"join_leave"]=0
  }
  return(nextBlock)   
}

for(i in 1:5){
  setwd(baseDir)
  setwd(wdList[i])
  curatedFile=curatedFileList[i]
  trackingFile=outList[i]
  
  curated=read.table(curatedFile, sep=",", as.is=T, header=T)
  fliesTracking=read.table(trackingFile, sep=",", as.is=T, header=T)
  fliesTracking[is.na(fliesTracking[,"nFlies"]), "nFlies"]=0
  
  frame=curated$minutes*1800+curated$seconds*30
  curated=cbind(curated, frame)
  for(i in 2:nrow(curated)){
    if((curated[i,"minutes"]==curated[i-1,"minutes"]) & (curated[i, "seconds"]==curated[i-1, "seconds"])){
      curated[i,"frame"]=curated[i-1,"frame"]+3
    }
  }
  curatedLast=curated[nrow(curated),]
  curatedLast$frame=108000
  aaa=which(curated[2:nrow(curated),"frame"] <= curated[1:(nrow(curated)-1),"frame"])
  bbb=aaa-1
  ccc=c(aaa, bbb)
  ccc=ccc[order(ccc)]
  curated[ccc,]
  
  curated=rbind(curated, curatedLast)
  curated1=curated[1:(nrow(curated)-1),"frame"]
  curated2=curated[2:nrow(curated),"frame"]
  sum(curated2<=curated1)
  
  curatedByFrame=do.call(rbind, lapply(1:(nrow(curated)-1), function(k) {addIntervening(nextLeave=curated[k+1,], lastLeave=curated[k,])}))
  curatedByFrame=rbind(curatedByFrame, curated[nrow(curated),])
  totFlies=curatedByFrame$nmales+curatedByFrame$nfemales
  time=curatedByFrame$frame/1800
  curatedByFrame=cbind(curatedByFrame, totFlies, time)
  
  write.table(curatedByFrame, "filledCurated.csv", sep=",", row.names=F, quote=F)
  
}


for(i in 1:5){
  setwd(baseDir)
  setwd(wdList[i])
  
  curatedFile=curatedFileList[i]
  trackingFile=outList[i]  
  curatedByFrame=read.table("filledCurated.csv", sep=",", as.is=T, header=T)
  fliesTracking=read.table(trackingFile, sep=",", as.is=T, header=T)
  fliesTracking[is.na(fliesTracking[,"nFlies"]), "nFlies"]=0
  
  count=1
  fliesTracking=cbind(fliesTracking, count)
  fliesTracking[which(fliesTracking[,"nFlies"]==0),"count"]=0
  totFliesTracking=aggregate(fliesTracking$count, by=list(fliesTracking[,"frame"]), FUN="sum")
  colnames(totFliesTracking)=c("frame", "nFlies")
  #nMeasures=nrow(totFliesTracking)
  time=totFliesTracking[,1]/1800
  totFliesTracking=cbind(totFliesTracking, time)
  
  mergEntProps=function(k,fliesTracking, cureABit){
    thisLine=data.frame(frame=k, minutes=cureABit[,"minutes"],seconds=cureABit[,"seconds"], nmales=cureABit[,"nmales"], nfemales=cureABit[,"nfemales"], 
                        joinLeave=cureABit[,"join_leave"], curateFlies=cureABit[,"totFlies"], trackedFlies=fliesTracking[,"nFlies"])
    return(thisLine)
  }
  
  mergedFile=do.call(rbind, lapply(1:108000, function(k){mergEntProps(k, fliesTracking=totFliesTracking[max(which(totFliesTracking[,"frame"]<=k)),], cureABit=curatedByFrame[max(which(curatedByFrame[,"frame"]<=k)),])}))
  write.table(mergedFile, "comparisonFile.csv", sep=",", col.names=T, row.names=F, quote=F)
  
}


for(i in 1:5){
  setwd(baseDir)
  setwd(wdList[i])
  mergedFile=read.table("comparisonFile.csv", sep=",", as.is=T, header=T)
  minFrame=0
  maxFrame=107999
  
  minVal=min(mergedFile[,c(7,8)])
  maxVal=max(mergedFile[,c(7,8)])
 
  png("blue_is_true_threshold.png", width=3000, height=1000)
  plot(mergedFile[minFrame:maxFrame,"trackedFlies"]~mergedFile[minFrame:maxFrame,"frame"], col="red", pch="-", ylim=c(), cex=6, cex.axis=4, cex.lab=4)
  points(mergedFile[minFrame:maxFrame,"curateFlies"]~mergedFile[minFrame:maxFrame,"frame"], col="blue", pch="-", cex=6, cex.axis=4, cex.lab=4) 
  #dev.copy(png, "blue_is_true_threshold.pdf", width=15, height=5)
  dev.off()
  
  cor(mergedFile[,"trackedFlies"],mergedFile[,"curateFlies"])
  mean(abs(mergedFile[,"trackedFlies"]-mergedFile[,"curateFlies"])/(mergedFile[,"curateFlies"]+1))
  
}


#####################
##Need to get directionality and frequency deviations from true for each of these
##########################



countBlobs=function(frameData){
  if(nrow(frameData)>0){
  nBlobs=max(frameData[,"blobIndex"])
  }else{
    nBlobs=0
  }
    
  return(nBlobs)
}


freqBias=data.frame()

for(i in 1:5){
  setwd(baseDir)
  setwd(wdList[i])
  mergedFile=read.table("comparisonFile.csv", sep=",", as.is=T, header=T)
  mergedFile=mergedFile[,1:8]
  outFile=read.table(outList[i], sep=",", as.is=T, header=T)
  maxFrame=max(outFile[,"frame"])
  myBlobs=do.call(rbind, lapply(1:maxFrame, function(k) {countBlobs(frameData=outFile[which(outFile[,"frame"]==k),])}))
  mergedFile=cbind(mergedFile[1:length(myBlobs),], myBlobs)
  
  blobDeltas=mergedFile[1:(nrow(mergedFile)-1),"myBlobs"] != mergedFile[2:(nrow(mergedFile)),"myBlobs"]
  blobDeltas=blobDeltas[!is.na(blobDeltas)]
  blobDelta=sum(blobDeltas)/length(blobDeltas)
  blobError=mean(abs(mergedFile[,"myBlobs"]-mergedFile[,"curateFlies"])/(mergedFile[,"curateFlies"]+1), na.rm=T)
  blobBias=mean((mergedFile[,"myBlobs"]-mergedFile[,"curateFlies"])/(mergedFile[,"curateFlies"]+1), na.rm=T)
  
  tabuDeltas=mergedFile[1:(nrow(mergedFile)-1),"trackedFlies"] != mergedFile[2:(nrow(mergedFile)),"trackedFlies"]
  tabuDeltas=tabuDeltas[!is.na(tabuDeltas)]
  tabuDelta=sum(tabuDeltas)/length(tabuDeltas)
  tabuError=mean(abs(mergedFile[,"trackedFlies"]-mergedFile[,"curateFlies"])/(mergedFile[,"curateFlies"]+1), na.rm=T)
  tabuBias=mean((mergedFile[,"trackedFlies"]-mergedFile[,"curateFlies"])/(mergedFile[,"curateFlies"]+1), na.rm=T)
  
  
  realDeltas=mergedFile[1:(nrow(mergedFile)-1),"curateFlies"] != mergedFile[2:(nrow(mergedFile)),"curateFlies"]
  realDeltas=realDeltas[!is.na(realDeltas)]
  realDelta=sum(realDeltas)/length(realDeltas)
  
  errorVector=data.frame(blobE=blobError, tabuE=tabuError, blobD=blobBias, tabuD=tabuBias, realJump=realDelta, blobJump=blobDelta, tabuJump=tabuDelta)
  freqBias=rbind(freqBias, errorVector)
  
  blobBiasVec=c((mergedFile[,"myBlobs"]-mergedFile[,"curateFlies"])/(mergedFile[,"curateFlies"]+1))
  tabuBiasVec=c((mergedFile[,"trackedFlies"]-mergedFile[,"curateFlies"])/(mergedFile[,"curateFlies"]+1))
  mergedFile=cbind(mergedFile,blobBiasVec,tabuBiasVec)
  
  write.table(mergedFile, "comparisonFile.csv", sep=",", col.names=T, row.names=F, quote=F)
  
}

errorEgress=c()
for(i in 1:5){
  setwd(baseDir)
  setwd(wdList[i])
  mergedFile=read.table("comparisonFile.csv", sep=",", as.is=T, header=T)
  errorEgress=rbind(errorEgress, mergedFile)
}


naffRegress=glm(errorEgress$tabuBiasVec~errorEgress$curateFlies)
summary(naffRegress)

blobRegress=glm(errorEgress$blobBiasVec~errorEgress$curateFlies)
summary(blobRegress)




par(mfrow=c(1,2))
par(mar=c(5,5,4,2))
smoothScatter(errorEgress$blobBiasVec~errorEgress$curateFlies, nbin=128, transformation=function(x) x^0.25, ylim=c(-0.6, 1.5), colramp=colorRampPalette(c("blue4", "skyblue", "yellow", "gold")), xlab="", ylab="", cex.axis=2)
rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col ="blue4")
smoothScatter(errorEgress$blobBiasVec~errorEgress$curateFlies, add=T, nbin=128, transformation=function(x) x^0.25, ylim=c(-0.6, 1.5), colramp=colorRampPalette(c("blue4", "skyblue", "yellow", "gold")), xlab="", ylab="", cex.axis=2)
title(main="Blob", ylab="Directionality", xlab="Flies on Patch", cex.lab=2, cex.main=2)
abline(blobRegress, col="darkgrey", lwd=14)
smoothScatter(errorEgress$tabuBiasVec~errorEgress$curateFlies, nbin=128, transformation=function(x) x^0.25, ylim=c(-0.6, 1.5), colramp=colorRampPalette(c("blue4", "skyblue", "yellow", "gold")), xlab="", ylab="", cex.axis=2)
rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], col ="blue4")
smoothScatter(errorEgress$tabuBiasVec~errorEgress$curateFlies, add=T, nbin=128, transformation=function(x) x^0.25, ylim=c(-0.6, 1.5), colramp=colorRampPalette(c("blue4", "skyblue", "yellow", "gold")), xlab="", ylab="", cex.axis=2)
title(main="TABU", xlab="Flies on Patch", cex.lab=2, cex.main=2)
abline(naffRegress, col="darkgray", lwd=14)
#hist(blobRegress$residuals)
#hist(naffRegress$residuals)
dev.copy(png, file="biasplot.png", width=3000, height=1600)
dev.off()

