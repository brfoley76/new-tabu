./tracking_code/ has the TABU algorithms in it
./videos/ has the output from TABU

./compCuratedTracked.r takes all the TABU outputs, and compares them with the student curated data, and outputs two file. 
	./*/comparisonFile.csv puts the TABU and student data in columns next to each other
		frame = frame number
		minutes = minutes into the video
		seconds = seconds into the video
		nmales = number of males students counted in a video at a given point
		nfemales = " females
		joinLeave = actual joins and leaves where join = 1, leave = -1
		curateFlies = student curated data, total number of flies 
		trackedFlies = TABU output number of flies		

	./*/filledCurated the student curated data, but transformed from mins and seconds to estimated per frame
		all variables as above except:
		colour = inferred colour of dot
		totFlies = same as curateFlies
		time = minutes (with decimal fractions)
	
	It also has the code tagged on at the end to do the smoothed scatterplots (the figure in the paper).
