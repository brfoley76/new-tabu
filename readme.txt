There are two folders: ./writeup contains the LaTex and figure files for the writeup. ./analysis contains the data and R code we need to run TABU.

./writeup:
	./ The main .tex file is called TabuJaaba_mm_dd.tex
	./ a bunch of necessary (?) .sty files are here
	./plots/ contains all plots
	./multirow/ contains some necessary formatting styles

./analysis:
	./TABU/videos/ DOES NOT contains the 5 hours of video we use for this analysis. 
	./TABU/videos/ but does contain relevant flytracker output. Each subfolder is from a different day and a different expt, named accordingly.
	./TABU/tracking_code/ contains the r scripts I use to run TABU
		The only one we need to concern ourselves with is ./tracking_code/RunScripts.r
		see readme.txt for details
	./readme.txt contains description of content of the daily expt files
	./JAABA/ contains Hetian's files, to be readmeed later
