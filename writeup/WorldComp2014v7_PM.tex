\documentclass[12pt]{article}
\usepackage[margin=2.5cm]{geometry}
\usepackage{parskip}
\usepackage{lineno}

\linenumbers
\modulolinenumbers[2]

\usepackage{mathpazo}
\usepackage{verbatim}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage[usenames,dvipsnames]{color}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\green}[1]{\textcolor{green}{#1}}
\newcommand{\seagreen}[1]{\textcolor{SeaGreen}{#1}}
\newcommand{\purple}[1]{\textcolor{Purple}{#1}}
\newcommand{\ignore}[1]{}
\usepackage{cite}
\usepackage{subfig}
\usepackage{enumitem}
\usepackage{array}
\usepackage{caption}

\usepackage{multirow}

\usepackage{enumerate}
\usepackage{setspace}
%\doublespacing
%opening
\begin{document}
\setlength{\parskip}{2pt}
\setlength{\parindent}{20pt}


%\title{xxx}

\title{Trajectory evaluation and behavioral scoring using JAABA in a noisy system}

\author{H. Chen, R. Ardekani, P. Marjoram, B.R. Foley}



\maketitle

Corresponding author: B. Foley - bradfole@usc.edu

Postal addresses: H. Chen and P. Marjoram: Dept.\ of Preventive Medicine, Keck School of Medicine, USC,  Los Angeles, California 90089, USA. (HC: hetianch@usc.edu; PM: pmarjora@usc.edu)\\
B. Foley and R. Ardekani: Molecular and Computational Biology, Dept. of Biological Sciences, USC, Los Angeles, California 90089, USA. (BF: bradfole@usc.edu, RA: dehestan@usc.edu)\\


\begin{abstract}
Typical methods for generating and analysing tracking data for animals such as \emph{Drosophila} require idealized experimental conditions that are frequently difficult,  expensive, or undesirable to replicate in the lab. In this paper we describe and implement methods for improving robustness in non-ideal conditions. Our method involves an initial processing step in which tracks are constructed from noisy video data, followed by a subsequent application of machine learning algorithms to further improve performance. We demonstrate that our methods are capable of generating a substantial improvement in performance in fly identification, and allow for effective construction of tracks for individual flies. We then use this data to train sex and behavior classifiers, which we employ to detect and describe behavioral differences among test experiments. As such, our algorithm provides a path for groups who wish to track fly, or characterize their behavior, in less than ideal conditions. 
 
 \vspace*{0.1in}
 Keywords: tracking, machine learning, JAABA, behavior
 
 \vspace*{0.1in}
 

\end{abstract}

\vspace*{-0.3in}
\section*{Introduction}\label{s:intro}
\vspace*{-0.15in}

Behavioral studies commonly rely upon extensive time-series observation of animals, and characterization of their movement, activities and social interactions. Historically this involved scientists (or their students) recording observations by hand---a laborious and error-prone process. More recently, automation has promised to dramatically increase the quantity and detail of data collected, and a variety of methods have recently become popular in the important area of automated tracking - for example, the CTRAX ethomics software \cite{ctrax}, and the proprietary Ethovision \cite{ethovision}. 

Most available solutions demand restricted experimental conditions that may not be desirable for the question of interest, or feasible in the field, (or even the lab). For example, in \emph{Drosophila melanogaster} experiments, it is common to restrict the possibility of flight, and use a backlit glass substrate for contrast \cite{ctrax}. A majority of \emph{D. melanogaster} social interactions occur on food, and glass is not representative of their normal habitat. Additionally, many tracking algorithms perform poorly when the number of objects being tracked is not fixed. In such contexts it is difficult to determine whether a large ``blob'' of pixels in fact represents a single object or two overlapping objects. Such close contact happens commonly during aggression, courtship and mating events. 

We are particularly interested in describing spontaneous group assembly, and describing the resultant behavior in those groups. That is, we need to analyse setups with variable numbers of flies that frequently come into close contact. As a test-case, we consider data from a set of experiments in which we recorded fly behavior in an environment consisting of four food patches, modelled on a published experiment conducted with still cameras \cite{Saltz:2011}. Each patch was recorded independently, and flies could freely move among patches, or be off patch (and thus not recorded). To model group assembly, we need to accurately count individuals on patches, and measure joining and leaving. We are currently able to detect objects (blobs, or putative flies) in video frames against a static background. This method is designed to be relatively robust to non-optimal experimental conditions.

Behavioral annotation requires that we move from static blobs, to individual-fly identification and tracking. Here, we build upon our work presented in \cite{ChenMarjoram:2014}, and describe a 3-stage process from video processing to behavioral annotation. First, we present an algorithm that enables us to assemble trajectories even through multi-fly blobs. Second, we then utilise these trajectories in freely-available machine-learning behavioral annotation software. The Janelia Automatic Animal Behavior Annotator (JAABA) is a commonly used animal-behavior annotation software \cite{jaaba}. We use JAABA to manually flag errors in our tracking algorithm for ``single fly'' versus ``multifly'' blobs. This enables subsequent trajectory correction and behavioural annotation. Finally, from the subset of trajectories consisting of high-likelihood single-fly blobs, we train a sex classifier to distinguish males from females. We also train a chasing classifier, which together with sex annotation allows us to score important social behaviors, namely courtship and aggression.

\vspace*{-0.15in}

\section*{Methods}\label{s:methods}
\vspace*{-0.15in}
{\bf Initial Video Processing:}
Videos are recorded using 4 high-resolution Grasshopper digital video cameras (Point Grey Research Inc., Richmond, Canada)  simultaneously filming individual patches at 30hz, RGB, and 800$\times$600 resolution. Videos are processed as single frames, by identifying blobs against an averaged background \cite{ArdekaniTavare:2013}. Blobs may contain from one to many individual flies, or be spurious artefacts. Features of the blobs are extracted using the cvBlobslib package  \cite{opencv}. The borders of the patch are defined manually, using the ImageJ software ellipse tool \cite{imagej}, and are calculated as length of the radius from centroid of the patch. All flies outside this radius are considered ``off patch''. Lighting was ambient room lighting. Videos were recorded for one hour intervals, and a subset were scored for joining and leaving by hand, to evaluate accuracy. 

To facilitate tracking and individual-fly (sex and genotype) identification, we painted a small color-coded dot on each fly (either blue or yellow). For color detection problems there are drawbacks to the RGB color space. First, there is a high correlation between red, green and blue channels; moreover, they are not robust to lighting conditions. In many situations one needs to be able to separate the color components from intensity~\cite{Saber1996,Sural2002,Park2012}. There are many linear and nonlinear color spaces that achieve better color constancy, effective bandwidth reduction, and robust color image processing~\cite{Wyszecki1982}. In particular, HSV has been shown to have better performance than RGB space for color recognition~\cite{Park2012}. For color recognition on silhouettes, RGB values of each pixel were converted to HSV using the equations of \cite{Wyszecki1982}. We selected thresholds in 3D HSV space, which were most effective for distinguishing blob colors. Each pixel within the fly silhouette was assigned to a color based on $h$, $s$ and $v$ values, and pixels which were not within these thresholds were disregarded. The paint color was inferred by the majority of pixels.

Blobs are identified for each frame, or time $T$. The number of blobs, and blob statistics for each $T$, were output. Blob statistics include the blob X and Y centroids ($B_X$ and $B_Y$); fitted-ellipse major and minor axes ($B_A$, $B_B$); and blob area (in pixels, $B_P$). Blobs with centroids outside the perimeter of the patch are excluded. Every blob is assigned a unique identifier within a frame ($B_i$). Each blob is subsequently assigned an inferred fly number ($B_n$, below).


\vspace*{0.05in}
\noindent{\bf Blobs and flies  -  Trajectory Assembly with Blob Uncertainty [TABU] :} Flies are taken to be nonfissible blob units. We infer the number and identity of flies within blobs by tracking fusion and fission events. We construct tracks by making three simplifying assumptions (based on observation). First, that flies do not often move a greater distance than their body length between frames. Second, that the noise in blob area estimation is not large between consecutive frames (\emph{i.e.,} less than half the actual fly area). Third, (on the scale of 30 frames a second) that flies do not join and leave patches often---that is, we conservatively assume fly number does not change, unless there is no other explanation for the data. TABU is implemented in R \cite{R}.

Trajectories are constructed by initializing the first frame assuming each blob is a single fly. Subsequently we implement the following algorithm at each frame:


 \textbf{1. Identify Neighborhoods:} For each pair of frames $T_{t}$ and $T_{t+1}$, we construct a graph by drawing edges between blobs that: a) are in different frames; and b) overlap. We define overlapping as having centroids within the distance of the major axis $B_A$ of the blob ellipse. We define two degrees of overlapping: mutual and unidirectional. A mutual overlap occurs when the $B_A$ of both blobs is longer than the distance between their centroids. If only one $B_A$ is this long, the overlap is unidirectional. A ``neighborhood'' is defined as group of blobs linked by mutual edges.

 \textbf{2. Check ``Joins" and ``Leaves":} We test for probable joining and leaving events by examining blobs that are not in a neighborhood with other blobs, using the more-relaxed unidirectional overlap. Flies in blobs in $T_{t}$ with no unidirectional matches in $T_{t+1}$ are assumed to have left, and flies in blobs in $T_{t+1}$ with no unidirectional matches in $T_{t}$ are assumed to have joined. Otherwise, the unmatched blobs are assigned to their nearest unidirectional match.

 \textbf{3. Assign flies to blobs:} In the simplest case, a neighborhood is comprised of a single blob in $T_t$ and $T_{t+1}$. If so, all flies in the the blob at $T_t$ are assigned to the blob at $T_{t+1}$. In more complex cases we assign flies between blobs to minimize the difference between summed fly-area and their respective $B_P$. Every fly inherits the blob-specific statistics of its assigned blob. During fission events if there are fewer flies than blobs we update fly numbers. Thus, we arrive at our count of flies. Each blob is also assigned a count of the number of flies it contains, $B_N$.

 \textbf{4. Update statistics:} Each fly is assigned a number of fly-specific statistics. These include a unique index for each fly ($F_j$); fly area in pixels ($F_P$); and fly area from the fitted ellipse ($F_e= {B_A B_B \pi}$). Statistics are running averages, updated only when a fly is inferred to be in a single-fly blob. An error parameter is also updated ($F_S$) to alert us when there is a mismatch between observed blob properties and the inferred fly state---for instance, if the ratio between $F_P$ and $F_e$ is much smaller than 0.9, there is a high likelihood the blob contains multiple flies. 

 \textbf{5. Resolve probable errors:} For cases where error deviance $F_S$ has grown too large, we attempt to reduce mismatch between imputed fly and blob matches by imputing leaving events, or evaluating group assignment.


\vspace*{0.05in}
We have found that this method gives us correct fly counts in blobs \textgreater85\% of the time, but is subject to several systematic biases (see Results). For example it deals poorly with occlusion due to mounting which may last for seconds, and mating, which lasts for up to 20 minutes. It also may incorrectly infer several small flies instead of a single large fly. 
We therefore attempt a subsequent analysis aimed at correcting these remaining biases using ML.

\vspace*{-0.15in}
\subsection*{Machine Learning [ML] in JAABA and Trajectory Scoring}
\vspace*{-0.12in}
Once TABU has been applied, the trajectories become compatible with JAABA, allowing us to conveniently score behavior using its video annotation capabilities. 
We then fit various ML algorithms. The first, GentleBoost, is natively implemented within JAABA. The others (GradientBoost, logistic regression, and Support Vector Machine [SVM] with linear and Gaussian kernels [gSVM and lSVM]) we implemented ourselves using the Python Scikit-learn \cite{scikit} package. For boosting we use decision stumps as the weak rules, and to ensure fair comparison default parameter values were used for all other methods. 


\textbf{Training of ML Algorithms:} 
We used JAABA to calculate a number of internal single-frame fly statistics, as well as multi-frame window features. Window features are normalized to have mean 0 and variance 1. It is these features that were used for the ML classifiers. Users define behaviors, and score positive and negative cases for trajectories in the JAABA GUI, by observation in the video window. Since the ML algorithms are binary classifiers, we scored instances of behavior as a binary outcome: Multifly = 1 for blobs labelled as having more than one fly, Multifly = 0 otherwise; Sex = 0 for female (or 1 for male); and Chase = 1 (or 0 for other behaviors). 

We then fit ML classifiers using 3-fold cross-validation analysis in which the training data uses the manual annotations that we input using JAABA. After fitting, the performance of each model was evaluated using accuracy, specificity, sensitivity, precision, and area under the curve. Here, accuracy is defined as the proportion of times that the fly state is correctly called, for a total number of validation calls. All other performance measures follow the usual statistical definitions. Sex and Multifly classifiers were were trained on 4000 frames from a single training video, and evaluated on 400 randomly picked frames. The Chase classifier was trained on 2000 frames, and evaluated on 500. At the same time, using the Multifly classifier, we evaluated the performance of the TABU input trajectories by scoring whether our $B_N$ statistic accurately described blob fly count. 

Sex classification was performed after trajectory scoring, and incorporated both an ML classifier (as above) and color information. Because the the color scoring was more accurate than the Sex classifier, integration was performed by applying the Sex classifier to frames where the color of the painted marker is uncertain.

In order to understand the biological significance of the Chase classifier, we need to understand the sex of the individuals involved in the behavior. Courtship and aggression are known to be important components of fly social behavior \cite{Saltz:2011}. These are usually male initiated, often characterised by chasing, and are directed at females and males respectively. Females rarely if ever chase other flies. Taking Chase together with Sex and Multifly, we created two composite behaviors: Aggression and Courtship.  In the case where Multifly = 0, Sex = 1, and Chase = 1, Aggression was defined as a male chasing another male, and Courtship was defined as a male chasing a female. To compare our test videos, we created a composite behavior profile for each. That profile comprised of the percentage of frames that a) contained Multifly blobs; b) contained at least one female; c) contained Aggression; and d) contained Courtship.

\vspace*{-0.15in}
\section*{Results}\label{s:results}

\vspace*{-0.15in}
We begin by evaluating the performance of the basic blob-recognition algorithm from \cite{ArdekaniTavare:2013}, and the change in accuracy after processing the data with TABU, for the basic task of recognizing fly number and for joining and leaving events. The  empirical 'real' results are obtained from manual annotation. Results are shown in Table~\ref{BlobTabu}. Let $e$ be the estimated number of flies in a frame for a given method, $n$ be the actual (manually annotated) number, and $\tau$ be the total number of frames. We estimate overall counting error, $E$, as $E=\frac{1}{\tau}\sum \limits_\tau \frac{|e-n|}{n+1}$ (where the denominator is $n+1$ to avoid division by zero). This represents an approximate per-fly probability of being miscounted.  Directionality, $D$, is calculated similarly, $D=\frac{1}{\tau}\sum \limits_\tau \frac{e-n}{n+1}$, and demonstrates the chances of being consistently over- or under-counted. Joining or leaving events, 'Jump', are reported as the per-frame probability of either a change in blob number, or a trajectory starting or ending. Results are shown for 5 separate videos ('Rep').


\begin{table}[ht]
\vspace*{-0.15in}
\centering
\caption{\small{Performance of the blob algorithm output (Blob), and TABU trajectory output. Counting error ($E$), and Directionality ($D$) bias in counting is shown. Empirical (Real) and estimated fly patch-joining or leaving rates (Jump) are also shown for raw blob data and processed trajectories.}} 
\label{BlobTabu}
\vspace{0.05in}
\begin{tabular}{lccccccc}
	\hline
	Rep & Blob $E$ & TABU $E$ & Blob $D$ & TABU $D$ & Real Jump& Blob Jump & TABU Jump \\
	\hline

1 & $0.177$ & $0.106$ & $-0.145$ & $0.074$ & $0.013$ & $0.085$ & $0.031$ \\
2 & $0.138$ & $0.197$ & $-0.101$ & $0.192$ & $0.009$ & $0.116$ & $0.017$ \\
3 & $0.156$ & $0.106$ & $0.077$ & $0.036$ & $0.011$ & $0.023$ & $0.014$ \\
4 & $0.110$ & $0.090$ & $0.025$ & $0.048$ & $0.003$ & $0.023$ & $0.015$ \\
5 & $0.123$ & $0.124$ & $-0.074$ & $0.098$ & $0.012$ & $0.127$ & $0.042$ \\
mean & $0.141$ & $0.125$ & $-0.044$ & $0.090$ &  $0.010$ & $0.075$ & $0.024$\\
	
	\hline
\end{tabular}
\end{table}

By using TABU to create our trajectories, we have obtained more accurate data than was provided by the raw blob counts (Table~\ref{BlobTabu}). We have also greatly reduced the correlation between bias and fly number in our estimates (Figure~\ref{biasplot}). While the raw output overestimates the number of flies on a patch at low fly numbers, it tends to underestimate fly numbers when there are more flies on a patch (Blob bias: est=-0.034, df=442173, t=-309.1, P \textless 0.001). However, TABU does show evidence of a consistent bias towards over-counting, which becomes slightly stronger at high numbers of flies (Tabu bias: est=0.0075375, df=495300, t=71.67, P \textless 0.001). Application of the Tabu algorithm reduces the number of spurious patch joining and leaving events to about 30\% over the raw blob data (Table~\ref{BlobTabu}). However, even for the TABU output, the number of inferred joining and leaving events is still more than 2$\times$ the actual data, offering potential for improvement through subsequent application of ML.

\begin{figure*}
%\vspace*{-0.5in}
\begin{center}
\caption{\small{Heat map of the distribution of per-fly over- and under-counts ($D$) as function of the number of flies on a patch for each frame across 5 test videos.}}
\label{biasplot}
\includegraphics[width=12cm]{./plots/bias.pdf}
\end{center}
\end{figure*}

We now investigate whether application of ML methods to our TABU trajectories can identify miscalled blob counts $B_N$. Three-fold cross-validation model-fit results are shown in Table~\ref{PerformanceOnTrained}. Here algorithms were trained using a period of 10K frames. We see that all models have an accuracy above 0.98. The two SVM models rank highly on almost all metrics, while logistic regression ranks poorly on most metrics. While JAABA is not top ranked on any metric, we note that it performs very well overall.


\begin{table}[ht]
\centering
\caption{\small{Performance measures of ML algorithms for Multifly calling on 3-fold cross validation. The accuracy, sensitivity, specificity, and area under the curve scores are shown for each. Ranks among ML methods for each performance score are given in brackets.}} 
\label{PerformanceOnTrained}
\vspace{0.05in}
\begin{tabular}{lccccc}
	\hline
	Algorithms & Accuracy & Sensitivity & Specificity & Precision & AUC \\
	\hline
	JAABA &$ 0.994 (2)$   &   $0.994 (3)$    & $0.994 (2)$      & $0.994 (2)$    &    -    \\
	GradientBoost      & $0.988 (5)$   &   $0.989 (5)$      &$0.987 (3)$     & $0.987 (5)$    & $0.994 (4)$ \\
	Logistic & $0.989 (4)$   &   $0.993 (4)$    & $0.984 (5)$      & $0.985 (3)$    & $0.997 (3)$ \\
	lSVM         & $0.991 (3)$   &    $0.996 (1)$    & $0.985 (4)$     & $0.986 (4)$    & $0.998 (2)$ \\ 
	gSVM        & $0.995 (1)$   &   $0.996 (2)$   &   $0.995 (1)$     & $0.995 (1)$    & $0.998 (1)$ \\ 
	\hline
\end{tabular}
\end{table}


The critical practical question is whether models trained on one part of a video will be equally effective when applied to later periods of the same video, or to completely new video. Fly behavior is known to change over time, and varies among different genotypes and in different social contexts. 
We tested the performance of all algorithms on 4 videos that were not used in the training of the algorithm. This included different genotypes and sex ratios, as well as slightly different lighting and focus, than the algorithms were trained on. Results are shown in (Table~\ref{PerformanceOnIndependent}). The performance of all ML methods dropped slightly under these new conditions. All the ML methods improved upon the trajectory input data from TABU. The performance ranking of the ML algorithms remained broadly the same in this new data. The gSVM did very well, and logistic regression did relatively poorly. Again JAABA (GentleBoost) did very well overall.

\begin{table}[ht]
\centering
\caption{\small{Evaluation of the ML algorithm performance for Multifly calling on non-training videos. The minimum to maximum range of accuracy, sensitivity, and specificity scores are shown for each, in comparison with the input trajectory data (TABU).}} 
\label{PerformanceOnIndependent}
\begin{tabular}{lccc}
	\hline
	Algorithms  &  Accuracy  &  Sensitivity &  Specificity  \\ 
	\hline
	TABU  & $0.823-0.994$ &  $0.747-0.994$ &$0.884-0.995$ \\
	JAABA & $0.986-0.995$ &$0.982-0.994$  &  $0.989-0.996$ \\
	GradientBoost & $0.981-0.990$ & $0.979-0.993$  & $0.977-0.987$\\
	Logistic & $0.983-0.993$ & $0.979-0.992$  & $0.977-0.990$\\
	lSVM & $0.983-0.992$ & $0.978-0.998$ & $0.976-0.987$ \\
	gSVM & $0.984-0.993$ & $0.975-0.992$ &  $0.984-0.993$\\
	\hline
\end{tabular}
\end{table}



\subsubsection*{Sex Annotation}
\vspace*{-0.15in}

We found that the various ML algorithms had accuracies in Sex calling ranging between 0.73 and 0.89 (Table~\ref{sexCV}). Logistic regression, Gradient Boost and JAABA all had similar accuracies, above 0.88. The video blob-processing annotation based on the colored marker, however, was the most accurate of all the Sex classifiers (with an accuracy of 0.93). Integrating this classifier together with the JAABA scores improved the performance statistics for Sex calling to 0.95 and above.

\begin{table}[ht]
\centering
\caption{\small{Performance measures of the Sex classifier on 3-fold cross validation. The accuracy, sensitivity, specificity, precision and area under the curve scores are shown for each.}} 

\label{sexCV}
\begin{tabular}{lccccc}
	\hline
	Algorithms & Accuracy  & Sensitivity & Specificity & Precision & AUC \\
	\hline
	blobColor &$0.939 $ &$0.904 $ &$0.975 $ &$0.997  $&-\\
	JAABA &$ 0.883 $   & $0.913 $ & $0.852 $& $0.860 $    &    -    \\
	GradientBoost      & $0.882$   & $0.893 $ & $0.871 $      & $0.874 (5)$    & $0.953 $ \\
	Logistic & $0.893$   & $0.867$     & $0.919$      & $0.915$    & $0.952$ \\
	lSVM         & $0.816$   & $0.906$      & $0.727$     & $0.769$    & $0.949$ \\ 
	gSVM        & $0.731$   & $0.827$      & $0.635$      & $0.695$    & $0.884$ \\ 
	blobColor+JAABA      & $0.967$   & $0.947$      & $0.987$      & $0.986$    & -\\ 

	\hline
\end{tabular}
\end{table}

On the non-training data, the performance of the Sex classifier for JAABA was very similar to that on the trained video (with an accuracy above 0.90), suggesting that this classification is very robust (Table~\ref{sexGT}). Indeed, all ML methods however (particularly lSVM) performed somewhat better when applied to the non-training data, perhaps suggesting that the training dataset was a particularly difficult dataset for which to call sex. 

\begin{table}[ht]
\centering
\caption{\small{Evaluation of the Sex classifier performance on non-training videos. The mean accuracy, sensitivity and specificity scores across the videos are shown.}} 

\label{sexGT}
\begin{tabular}{lccc}
	\hline
	Algorithms  &  Accuracy   &  Sensitivity  &  Specificity \\ 
	\hline
	JAABA  & $0.919$ & $0.941 $ & $0.877 $ \\
	GradientBoost& $0.925$ & $0.942 $ & $0.895 $  \\
	 Logistic& $0.872$ & $ 0.940$ & $0.779 $  \\	
	 lSVM& $0.957$ & $ 0.932$ & $ 0.981$  \\
	 gSVM& $0.854$ & $ 0.848$ & $0.859 $  \\
	\hline
\end{tabular}
\end{table}


\subsubsection*{Behavior Annotation}

\vspace*{-0.15in}

The ultimate goal of trajectory analysis, and the implementation of JAABA on tracking data, is to evaluate behaviors in a diversity of experimental manipulations. In scoring Chasing, JAABA, GradientBoost, and logistic regression all performed well, with accuracy above 0.85 (Table~\ref{chaseCV}). Even on the non-training dataset, all the methods besides the gaussian SVM had accuracy greater than 0.8, but logistic regression and the linear SVM performed best (Table~\ref{chaseGT}). 

\begin{table}[ht]
\centering
\caption{\small{Evaluation of the Chase classifier  performance on 3-fold cross validation. The accuracy, sensitivity, specificity, precision and area under the curve scores are shown for each.}} 
\label{chaseCV}
\begin{tabular}{lccccc}
	\hline
	Algorithms & Accuracy  & Sensitivity & Specificity & Precision & AUC \\
	\hline
	JAABA &$0.887 $ &$0.920 $ &$0.867 $ &$0.809  $&-\\
	GradientBoost &$ 0.884 $   & $0.919 $ & $0.827 $& $0.896 $    &  $  0.917 $   \\
	Logistic & $0.859$   & $0.913$     & $0.770$      & $0.866$    & $0.888$ \\
	lSVM         & $0.844$   & $0.813$      & $0.895$     & $0.927$    & $0.861$ \\ 
	gSVM        & $0.781$   & $0.696$      & $0.919$      & $0.933$    & $0.885$ \\ 
	\hline
\end{tabular}
\end{table}

\begin{table}[ht]
\centering
\caption{\small{Evaluation of the Chase classifier performance on non-training videos. The accuracy, sensitivity, and specificity scores are shown for each.}}

\label{chaseGT}
\begin{tabular}{lccc}
	\hline
	Algorithms  &  Accuracy   &  Sensitivity  &  Specificity \\ 
	\hline
	JAABA  & $0.826$ & $0.949 $ & $0.764$ \\
	GradientBoost& $0.801$ & $0.989 $ & $0.750$  \\
	 Logistic& $0.871$ & $ 0.987$ & $0.766 $  \\	
	 lSVM& $0.880$ & $ 0.915$ & $ 0.839$  \\
	 gSVM& $0.762$ & $ 0.932$ & $0.668 $  \\
	\hline
	
\end{tabular}
\end{table}

Intriguingly, we found gross differences in fly behavior between the videos, which generally reflected the sex ratio composition of their respective experiments. It is precisely differences such as these that we hope we will  be able to detect  using methods such those we have developed here, so this is an encouraging result. A higher proportion of females results in a greater likelihood of observing females on patch, as well as an increased prevalence of Multifly blobs (Table~\ref{flyProfiles}). There was a trend towards more frequent aggression with an increase in male numbers, while courtship was observed more frequently at intermediate sex ratios. These results are entirely consistent with what we understand about fly behavior, which again provides encouragement regarding the usefulness of automated annotation of fly behavior such as that we present here.


\begin{table}[ht]
\centering
\caption{\small{Behavioral profiles of the 5 fly videos (v1---v5), with the number of females:males in the experiment (but not necessarily in frame) shown in brackets. The percentage of frames containing at least one of these behaviors is shown. Behavioral annotations were obtained using JAABA.}}

\label{flyProfiles}
\begin{tabular}{lccccc}
	\hline
	Behaviors  &  v1 (15:5)   &  v2 (15:5)  &  v3 (10:10)  & v4 (10:10) & v5 (5:15)\\ 
	\hline
	Multifly\%  & $32.61$ & $28.09 $ &$15.43$&$22.46$ & $17.23$ \\
	Sex = 0\%& $51.34$ & $63.54 $  &$22.75$&$44.72$& $24.28$ \\
	 Aggression\%& $0.29$ & $ 0.987$  &$1.40$ &$1.73$& $1.45 $\\	
	 Courtship\%& $0.66$ & $ 0.915$  &$0.82$&$1.65$& $ 0.73$ \\
	\hline
	
\end{tabular}
\end{table}



\vspace*{-0.15in}
\section*{Discussion}\label{s:discuss}
\vspace*{-0.18in}

In this paper we have developed a method for generating, and error correcting, tracking data from video recordings of {\em Drosophila} made in non-ideal  conditions. Non-optimal conditions cause problems at the initial image processing stage, due to poor performance of background subtraction routines, occlusion caused by proximity between animals, and uncertainty in the number of objects that need to be tracked (\emph{c.f.} \cite{ctrax}). This leads to subsequent poor performance of tracking data. However, imperfect conditions will  apply for a majority of behavioral observation systems in nature. Even in many lab situations, experimenters often have to work with such conditions to collect relevant data. Our methods offer the potential for investigators to more successfully work with such data.

Our simple TABU tracking algorithm, by making a few realistic assumptions about the persistence of flies across frames and within blobs, greatly reduces the uncertainty of the initial image processing data from the algorithm of \cite{ArdekaniTavare:2013}. It allows us to count flies on patches with more certainty, and reduces the apparent degree of fly movement on and off of patches. Error rates are still non-zero, but it is clear that subsequent application of any of the ML methods we tested here has the potential to increase correct allocation of flies among blobs from around 90\% to over 98\%. 

Among the algorithms we evaluated, there is no clear winner among the ML methods in terms of performance. However, for ease of implementation, and robustly high performance, the GentleBoost algorithm natively implemented in JAABA represents a reasonable choice for future work. It performed well for the Multifly classifier, and consistently well for Sex and Chase. We emphasize that use of JAABA (and all the behavioral annotation algorithms we tested) requires fly tracking data as input, thereby necessitating pre-processing using an algorithm such as TABU before use. Such a pre-processing algorithm needs to be able to construct tracks successfully in non-ideal conditions, and when the number of objects being tracked is unknown, a problem that is known to be extremely challenging \cite{ctrax}.

In the test videos, we were able to integrate multiple classifiers, such as painted marker color and Sex to improve our ability to score accurately. We were also able to integrate Sex and Chase in order to obtain richer biological data than was available from individual classifiers. We recapitulated the experimental sex ratio conditions (i.e. higher rates of observation of female prevalence correspond with higher female sex ratios) in the untrained videos. Importantly, we detected nteresting patterns in behavioral differences among the trials related to sex ratio---there were trends in the frequency of male-male Aggression, but not, interestingly, Courtship. It might be that as males spend more time fighting, they have less time or opportunity to court. The ability to detect such meaningful behavioral trends, in naturalistic setups, will be of great interest to ethologists.

In sum, our methods produce improved performance both in terms of accurate identification of the number of flies in a blob (and, therefore, the number of flies in a frame at any given moment), and in terms of generation of tracks for individual flies. Both of these types of information are crucial for analysis of fly (and other animal) group behavior. Flies are social animals, that actively aggregate and interact in groups. The sizes of these groups is therefore a key diagnostic of the behavior of those flies, and varies with factors such as  genotype, sex ratio, etc. Therefore, the methods we present here provide the opportunity for researchers to use automated methods to generate large quantities of such data in an experimental context. A more difficult remaining challenge is to automatically recognize interactions between flies, such as courtship and acts of aggression. Methods (including JAABA) are being developed to attack this problem. Creating, and error correcting, fly trajectories is a necessary first step in taking advantage of this work.

\vspace{0.1in}
\small{\noindent{\bf Acknowledgements:} The authors gratefully acknowledge funding from NSF and NIMH through awards  DMS 1101060 and MH100879. The material contained in this paper reflects the views of the authors, and not necessarily those of NSF or NMH.}

\vspace*{-0.15in}
\bibliography{MarjoramRefsMarch2014}


\bibliographystyle{plain}

\end{document}
