het1=read.csv("plot90.csv")
het1a=het1[,1:3]
het1b=het1[,4:5]

het1=merge(het1a, het1b, by.x="entire_frame", by.y="disagreement.frame", all.x=T)
rep=1
het1=cbind(rep, het1)

het2=read.csv("plot39.csv")
het2a=het2[,1:3]
het2b=het2[,4:5]

het2=merge(het2a, het2b, by.x="entire_frame", by.y="disagree_frame", all.x=T)
rep=2
het2=cbind(rep, het2)

het3=read.csv("plot12.csv")
het3a=het3[,1:3]
het3b=het3[,4:5]

het3=merge(het3a, het3b, by.x="entire_frame", by.y="disagree_frame", all.x=T)
rep=3
het3=cbind(rep, het3)

myData=rbind(het1, het2, het3)

myData[is.na(myData[,"TRUE."]),"TRUE."]=0
plusOne=myData[,"JAABA_pred"]
isNA=c(myData[,"TRUE."]==0)
myData[isNA,"TRUE."]=plusOne[isNA]+0.02

write.table(myData, "compMethods.table", sep=",", quote=F, row.names=F)
myData[,"TABU_pred"]=myData[,"TABU_pred"]+0.08
myData[,"TRUE."]=myData[,"TRUE."]+0.16



getwd()
myData=read.csv("hetian.data.csv")
myData[,3]=myData[,3]-0.05
myData[,4]=myData[,4]+0.05

par(mfrow=c(3, 1))
par(mar=c(4,5,0.1,2))
rep=c(12, 39, 91)
for(i in 1:3){
  thisData=myData[which(myData[,"rep"]==rep[i]),]
  thisData1=thisData[1:(nrow(thisData)-1),]
  thisData2=thisData[2:nrow(thisData),]
  thisIntervalData=cbind(thisData1, thisData2)
  plot(thisData[,"JAABA_pred"]~thisData[,"entire_frame"], pch=".", cex=1, col="white", ylim=c(-0.3, 1.3), xlab="", ylab="Blob State")
  #points(thisData[,"TABU_pred"]~thisData[,"entire_frame"], pch=15, cex=1, col="red")
  #points(thisData[,"TRUE."]~thisData[,"entire_frame"], pch=15, cex=1, col="black")
  
  for(j in 1:nrow(thisIntervalData)){
    segments(thisIntervalData[j,2],thisIntervalData[j,3], thisIntervalData[j,7],thisIntervalData[j,3], col="blue", lwd=6, lend=2)
    segments(thisIntervalData[j,2],thisIntervalData[j,4], thisIntervalData[j,7],thisIntervalData[j,4], col="red", lwd=6, lend=2)
    segments(thisIntervalData[j,2],thisIntervalData[j,5], thisIntervalData[j,7],thisIntervalData[j,5], col="black", lwd=6, lend=2)
  }

}
title(xlab="Frame Number")

dev.copy(png, file="agree.png", width=1000, height=600)
dev.off()


